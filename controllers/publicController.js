'use strict'

const { validate } = require('deep-email-validator');
const req = require('request');

const config = require("../config");
const { additionalTopLevelDomains } = require('../assets/constants');

const deepEmailValidatorAPI = async (request, response, next) => {
    let email = request.query.email;

    // Validation
    if (!email || typeof email !== 'string') {
        return response.status(400).json({ status: "error", message: 'Missing parameters.' });
    }

    // Validate smtp flag
    let validateSMTP = request.query && request.query.smtp ? true : false;

    // Do business logic
    try {
        let isDisposableEmailStatus = await isDisposableEmail(email);

        // Not a disposable email
        if (isDisposableEmailStatus === false) {
            // Can also be called with these default options
            let result = await validate({
                email: email,
                sender: email,
                validateRegex: true,
                validateMx: true,
                validateTypo: false,
                validateDisposable: true,
                validateSMTP: validateSMTP,
                additionalTopLevelDomains: additionalTopLevelDomains,
            });

            return response.status(200).send({ result });
        } else {
            // Prepare result object and add to resultsObj
            let resultObj = {
                "valid": false,
                "validators": {
                    "regex": {
                        "valid": true
                    },
                    "typo": {
                        "valid": true
                    },
                    "disposable": {
                        "valid": false
                    },
                    "mx": {
                        "valid": true,
                    },
                    "smtp": {
                        "valid": false
                    }
                },
                "reason": "Temporary email"
            };
            return response.status(200).send({ resultObj });
        }
    } catch (err) {
        // Print error message
        console.error(err);

        // Throw error
        return response.status(500).json({ status: "error", message: err.message });
    }
}

const deepEmailValidatorListAPI = async (request, response, next) => {
    let payload = request.body;

    // Do Validation
    if (!payload || !payload.emails || payload.emails.length === 0 || typeof payload.emails !== 'string') {
        // Throw error
        return response.status(400).json({ status: "error", message: 'Missing parameters.' });
    }

    // Do business logic
    try {
        let validateSMTP = payload.smtp ? true : false;
        let resultsObj = {};

        let emailsArray = payload.emails.split(',');

        for (var i = 0; i < emailsArray.length; i++) {
            let isDisposableEmailStatus = await isDisposableEmail(emailsArray[i]);

            // Not a disposable email
            if (isDisposableEmailStatus === false) {
                // Proceed with validation
                let result = await validate({
                    email: emailsArray[i],
                    sender: emailsArray[i],
                    validateRegex: true,
                    validateMx: true,
                    validateTypo: false,
                    validateDisposable: true,
                    validateSMTP: validateSMTP,
                    additionalTopLevelDomains: additionalTopLevelDomains,
                });
                resultsObj[emailsArray[i]] = result;
            } else {
                // Prepare result object and add to resultsObj
                let resultObj = {
                    "valid": false,
                    "validators": {
                        "regex": {
                            "valid": true
                        },
                        "typo": {
                            "valid": true
                        },
                        "disposable": {
                            "valid": false
                        },
                        "mx": {
                            "valid": true,
                        },
                        "smtp": {
                            "valid": false
                        }
                    },
                    "reason": "Temporary email"
                };
                resultsObj[emailsArray[i]] = resultObj;
            }
        }

        return response.status(200).send(resultsObj);
    } catch (err) {
        // Print error message
        console.error(err);

        // Throw error
        return response.status(500).json({ status: "error", message: err.message });
    }
}

function getDomainFromEmail(email) {
    let domain = "";
    let pattern = /@([^\s@]+)/;

    // Extract the domain name using the pattern
    var matches = email.match(pattern);

    // Check if a domain name match was found
    if (matches && matches.length > 1) {
        domain = matches[1];

        return domain;
    }
}

function getDisposableStatusFromCloudFlare(url) {
    return new Promise((resolve, reject) => {
        req.get(url, (error, response, body) => {
            if (error) {
                console.error(error);
                reject(error);
            } else {
                const result = JSON.parse(body)?.status ? JSON.parse(body)?.status : false;
                resolve(result);
            }
        });
    });
}

async function isDisposableEmail(email) {
    const domain = getDomainFromEmail(email);

    try {
        if (domain) {
            const result = await getDisposableStatusFromCloudFlare(config.disposableSvcUrl + "/get?id=" + domain);
            return result
        }
    } catch (error) {
        console.error(error)
    }

    return false;
}

module.exports = {
    deepEmailValidatorAPI,
    deepEmailValidatorListAPI
}