const dotenv = require("dotenv");
const assert = require("assert");

// Load config to process.env
dotenv.config();

// Get values from environment variable
const {
    PORT,
    HOST,
    HOST_URL,
    DISPOSABLE_SVC_URL
} = process.env;

// Validation
assert(PORT, "PORT is required");
assert(HOST, "HOST is required");

// Export
module.exports = {
    port: PORT,
    host: HOST,
    url: HOST_URL,
    disposableSvcUrl: DISPOSABLE_SVC_URL
}