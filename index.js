// Express framework
const express = require('express');
const cors = require("cors");
const bodyParser = require("body-parser");

// Load configuration
const config = require('./config');

// Load routes
const { routes: publicAPIRoutes } = require("./routes/public-routes");

const app = express();

app.use(express.json({ limit: '10mb' }));
app.use(express.urlencoded({ limit: '10mb', extended: true, parameterLimit: 10000 }));

app.use(cors());
app.use(bodyParser.json({ limit: "10mb" }));
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true, parameterLimit: 10000 }));

app.use("/api", [publicAPIRoutes]);

app.get("/", (req, res) => res.send("Hello there!"));

app.listen(config.port, () => { console.log("App is listening on url http://localhost:" + config.port) });