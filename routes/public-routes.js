// Load router from express
const express = require("express");
const router = express.Router();

// Load business logic
const { deepEmailValidatorAPI, deepEmailValidatorListAPI } = require("../controllers/publicController");

// Email validation
router.get("/email/check", deepEmailValidatorAPI);

//Deep email validator for list
router.post("/email/check", deepEmailValidatorListAPI);

module.exports = {
    routes: router
}